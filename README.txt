-- SUMMARY --

This module is for people who upgraded their Drupal 6 to Drupal 7 and found there is no D7 version of the Nodewords module. There is an alternative called Meta tags quick.

I built this simple module to provide conversion tool between the D6 Nodewords and D7 Meta tags quick.

-- REQUIREMENTS --

Drupal 7 migrated from Drupal 6 using Nodewords module
nodewords table from your previous Drupal 6 database.
Meta tags quick module enabled and installed

-- HOW TO USE IT --

Upgrade Drupal 6 to Drupal 7 as you usually do. Install and enable the Meta tags quick module. Then install and enable this module. Navigate to Administration > Configuration > Development > Convert Nodewords to meta tags quick.
Click Convert! button and wait for a while.

After the conversion is done, feel free to remove this module.

-- HOW IT WORKS --

To be able to do something this module needs the nodewords table from your previous Drupal 6 installation. It will get keywords and description metatags for each node from it, based on nid. Then it will save them to the metatags_quick table in Drupal 7 database structure. The operation is performed using he Drupal Batch API.

-- LIMITATIONS --

Only 255 character from the descriptions and keywords data are converted, since the Meta tags quick module doesn't offer to store larger strings.

-- CONTACT --
* Jan Polzer (maxiorel) - http://drupal.org/user/49016

This module was originally written for http://www.backuphowto.info/ website.